
[@ww.text name='cloudfoundry.task.servicePush.description' /]

[@ui.messageBox type="warning" titleKey="cloudfoundry.task.servicePush.beta"]
    [@ww.text name="cloudfoundry.task.servicePush.beta.message"]
    [/@ww.text]
[/@ui.messageBox]

[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.servicePush.section']
    [@ww.textfield labelKey='cloudfoundry.task.servicePush.fileName' name='cf_file' required='true' /]
    [@ww.url id='helpUrl' value='/serviceYamlHelp.action'/]
    <a href="${helpUrl}" id="yamlHelpLink">[@ww.text name='cloudfoundry.task.servicePush.yamlFile.description.help' /]</a>
[/@ui.bambooSection]

<script type="text/javascript">
 AJS.$(function() {
     AJS.$('#yamlHelpLink').click(function(event) {
          event.preventDefault();
          openHelp(this.href);
      });
     AJS.$('#issueTracker').click(function(event) {
          event.preventDefault();
          openHelp(this.href);
      });
 });
 </script>