/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.upgrade;

import static com.google.common.collect.Iterables.filter;
import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.isCloudFoundryApplicationTask;
import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.isCloudFoundryPushTask;
import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.isCloudFoundryServiceTask;
import static org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryPredicates.isJobWithCloudFoundryTask;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.gaptap.bamboo.cloudfoundry.PluginProperties;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.task.TaskConfigurationService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.collect.Maps;

/**
 * @author David Ehringer
 */
public class AddCfPrefixToTaskConfigKeysUpgrade implements PluginUpgradeTask {

    private static final Logger LOG = Logger.getLogger(AddCfPrefixToTaskConfigKeysUpgrade.class);

    private static final String PREFIX = "cf_";

    private final PlanManager planManager;
    private TaskConfigurationService taskConfigurationService;

    public AddCfPrefixToTaskConfigKeysUpgrade(PlanManager planManager) {
        this.planManager = planManager;
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        LOG.info("Executing AddCfPrefixToTaskConfigKeysUpgrade");

        for (Job job : filter(planManager.getAllPlans(Job.class), isJobWithCloudFoundryTask())) {
            BuildDefinition buildDefinition = job.getBuildDefinition();
            for (TaskDefinition taskDefinition : filter(buildDefinition.getTaskDefinitions(), isCloudFoundryPushTask())) {
                getTaskConfigurationService().editTask(job.getPlanKey(), taskDefinition.getId(),
                        taskDefinition.getUserDescription(), taskDefinition.isEnabled(),
                        getUpdatedConfigurationMap(taskDefinition), taskDefinition.getRootDirectorySelector());
            }
            for (TaskDefinition taskDefinition : filter(buildDefinition.getTaskDefinitions(),
                    isCloudFoundryServiceTask())) {
                getTaskConfigurationService().editTask(job.getPlanKey(), taskDefinition.getId(),
                        taskDefinition.getUserDescription(), taskDefinition.isEnabled(),
                        getUpdatedConfigurationMap(taskDefinition),
                        taskDefinition.getRootDirectorySelector());
            }
            for (TaskDefinition taskDefinition : filter(buildDefinition.getTaskDefinitions(),
                    isCloudFoundryApplicationTask())) {
                getTaskConfigurationService().editTask(job.getPlanKey(), taskDefinition.getId(),
                        taskDefinition.getUserDescription(), taskDefinition.isEnabled(),
                        getUpdatedConfigurationMap(taskDefinition),
                        taskDefinition.getRootDirectorySelector());
            }
        }

        LOG.info("AddCfPrefixToTaskConfigKeysUpgrade Complete");
        return null;
    }
    
    private Map<String, String> getUpdatedConfigurationMap(TaskDefinition taskDefinition) {
        Map<String, String> config = Maps.newHashMap();
        for (Entry<String, String> entry : taskDefinition.getConfiguration().entrySet()) {
            config.put(PREFIX + entry.getKey(), entry.getValue());
        }
        return config;
    }

    @Override
    public int getBuildNumber() {
        return 2;
    }

    @Override
    public String getPluginKey() {
        return PluginProperties.getPluginKey();
    }

    @Override
    public String getShortDescription() {
        return "Upgrades Task Configuration Data so that all fields are prefixed by \"" + PREFIX + "\".";
    }

    private TaskConfigurationService getTaskConfigurationService() {
        if (taskConfigurationService == null) {
            taskConfigurationService = (TaskConfigurationService) ContainerManager
                    .getComponent("taskConfigurationService");
        }
        return taskConfigurationService;
    }

}
