/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin;

import java.util.Collection;
import java.util.List;


/**
 * @author David Ehringer
 */
public interface CloudFoundryAdminService {

	Target addTarget(String name, String url, String description, boolean trustSelfSignedCerts, int credentialsId, Integer proxyId, boolean disableForBuildPlans);

	Target updateTarget(int id, String name, String url, String description, boolean trustSelfSignedCerts, int credentialsId, Integer proxyId, boolean disableForBuildPlans);

	Target getTarget(int id);

	List<Target> allTargets();

	void deleteTarget(int id);

	Collection<Credentials> allCredentials();

	Credentials addCredentials(String name, String username, String password,
			String description);

	Credentials getCredentials(int id);

	Credentials updateCredentials(int id, String name, String username,
			String password, String description);

	void deleteCredentials(int id);
	
	Collection<Proxy> allProxies();

	Proxy addProxy(String name, String host, int port,
			String description);

	Proxy getProxy(int id);

	Proxy updateProxy(int id, String name, String host, int port,
			String description);

	void deleteProxy(int id);
}
