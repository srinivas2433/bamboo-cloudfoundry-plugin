package org.gaptap.bamboo.cloudfoundry.client;

import org.junit.Test;

public class ReactorHealthCheckerTest {

    @Test(expected = IllegalArgumentException.class)
    public void anAppMustHaveRoutesForABlueGreenHealthCheckToBePerformed(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .customDarkAppName("green-app")
                .customDarkRoute("green.cf.com")
                .healthCheckEndpoint("/heath")
                .build();
        HealthChecker healthChecker = new ReactorHealthChecker();

        healthChecker.assertHealthy(applicationConfiguration, blueGreenConfiguration, new Log4jLogger());
    }
}
